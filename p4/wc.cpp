/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *Code Written by: Kenny Armenta
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 40
 */
#include<iostream>
using std::cin;
using std::cout;
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */

	string s = "" ;
	char a;
	enum states { ws, nws};
	int state = ws;
	int wordcount = 0, numoflines = 0, numofbytes = 0, maxlinelen = 0,
	linelen = 0;
	set<string> uword;

	while(fread(&a,1,1,stdin))
	{
		numofbytes++;

		if(a == ' ' && a == '\n')
		{
			s+=a;
		}
		else if(s != "" && (a == ' ' || a == '\n'))
			{
				uword.insert(s);
				s="";
			}

		if(a=='\t')
		{
			linelen += 8 - (linelen%8);
		}
		else if(a != '\n')
		{
			linelen++;
		}
		if(a == ' ' || a == '\t') //maintain ws
		{
			state = ws;
		}
		else if(a == '\n')
		{
			numoflines++;
			if(maxlinelen < linelen)
			{
				maxlinelen = linelen;
			}
			state = ws;
			linelen = 0;
		}
		else
		{
			if(!(state)) //!--Add ws >> Then set to nws--!
			{
				wordcount++;
			}
			state = nws;
		}
}
	if(linesonly)
		cout << "\t" << numoflines;
	if(wordsonly)
		cout << "\t" << wordcount;
	if(charonly)
		cout << "\t" << numofbytes;
	if(longonly)
		cout << "\t" << maxlinelen;
	if(uwordsonly)
		cout << "\t" << uword.size();
	if(!(charonly) && !(linesonly) && !(wordsonly) && !(longonly) && !(uwordsonly))
		cout << numoflines << "\t" << wordcount << "\t" << numofbytes;

	cout << "\n";

	return 0;
}

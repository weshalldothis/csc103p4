/*By Elvin and Aunik
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);

	/* TODO: finish this... */
	vector<char> echo_input;
	vector<char> s1_vec;
	vector<char> s2_vec;

	unsigned char u;
	while(fread(&u,1,1,stdin))
		{
			echo_input.push_back(u);
		}

	if(s1.empty() != 1)
	{
		for(unsigned int q=0;q<s1.size();q++)
		{
			if(s1[q]=='\\' &&s1[q+1]=='t')
			{
			s1_vec.push_back('\t');
				q++;
			}
			else if(s1[q]=='\\' &&s1[q+1]=='n')
			{
				s1_vec.push_back('\n');
				q++;
			}
			else if(s1[q]=='\\' &&s1[q+1]=='\\')
			{
				s1_vec.push_back('\\');
				q++;
			}
			else
			s1_vec.push_back(s1[q]);
		}
	}

	if(s2.empty() != 1)
	{
		for(unsigned int w=0;w<s2.size();w++)
		{
			if(s2[w]=='\\' &&s2[w+1]=='t')
			{
			s2_vec.push_back('\t');
				w++;
			}
			else if(s2[w]=='\\' &&s2[w+1]=='n')
			{
				s2_vec.push_back('\n');
				w++;
			}
			else if(s2[w]=='\\' &&s2[w+1]=='\\')
			{
				s2_vec.push_back('\\');
				w++;
			}
			else
			s2_vec.push_back(s2[w]);
		}
	}



	if(comp==0 && del==0)
	{
		for(unsigned int r=0;r<echo_input.size();r++)
		{
			for(unsigned int i=0;i<s1_vec.size();i++)
			{
				if(echo_input[r]==s1_vec[i])
				{
					if(s2_vec[i])
					{
						echo_input[r]=s2_vec[i];
					}
					else
						echo_input[r]=s2_vec[s2_vec.size()-1];
				}
			}
		}
		for(unsigned int y=0;y<echo_input.size();y++)
		{
			cout<<echo_input[y];
		}

	}
	else if(comp==1 && del==0)
	{
		bool change = true;
	for(unsigned int i=0;i<echo_input.size();i++)
		{
			for(unsigned int j=0;j<s1_vec.size();j++)
			{
				if(echo_input[i]==s1_vec[j])
				{
				change = false;
				break;
				}
				else
				{
				change = true;
				}
			}
			if(change)
			{
				echo_input[i]=s2_vec[s2_vec.size()-1];
			}
		}
		for(unsigned int i =0;i<echo_input.size();i++)
		{
			cout<<echo_input[i];
		}
	}
	else if(comp==0 && del==1)
	{
		vector<char> out_put;
		bool print=false;
		for(unsigned int i=0;i<echo_input.size();i++)
		{
			for(unsigned int j=0;j<s1_vec.size();j++)
			{
				if(echo_input[i]!=s1_vec[j])
				{
					print=true;
				}
				else if(echo_input[i]==s1_vec[j])
				{
					print=false;
					break;
				}
			}
			if(print)
			{
			out_put.push_back(echo_input[i]);
			}
		}


		for(unsigned int y=0;y<out_put.size();y++)
		{
			cout<<out_put[y];
		}
	}
	else if(comp==1 && del==1)
	{
	vector<char> out_put;
	bool save = true;
	for(unsigned int i=0;i<echo_input.size();i++)
		{
			for(unsigned int j=0;j<s1_vec.size();j++)
			{
				if(echo_input[i]==s1_vec[j])
				{
				save = true;
				break;
				}
				else
				{
				save= false;;
				}
			}
			if(save)
			{
				out_put.push_back(echo_input[i]);
			}
		}
		for(unsigned int i =0;i<out_put.size();i++)
		{
			cout<<out_put[i];
		}
	}

	return 0;
}

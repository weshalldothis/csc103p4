/********************************
This code was written by:
Elvin Martinez

Lab section:
MM1
*********************************/


#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
using std::to_string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e': //everything after -e is considered as input for cin
				echo = 1;
				break;
			case 'i': //shuffle every number between lower#-higher#
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg); //limit the output to onlyn lines
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */
	vector<string> words;
	int y;
	if (echo==1 && userange!=true)
	{
	while(optind<argc)
		words.push_back(argv[optind++]);
	}
	else if(echo==0 && userange==true)
	{
		string str;
		y =rlow;
		for(int r=0;r<=(rhigh-rlow);r++)
		{
			str=to_string(y);
			words.push_back(str);
			y++;
		}
	}

	else if(echo==0 && userange!=true)
	{
		string input;
		while(getline(cin,input))
		{
			words.push_back(input);
		}
	}
	else if (echo==1 && userange==true)
	{
		cout<<"Error: Cannot use both arguments\n";
		return 1;
	}
	else
	{
		cout<<"Error\n";
		return 1;
	}

	int range =2;/*should be same size as vector storing the words*/
	range=words.size();

	/*Sets non-repeating random numbers to a vector the same size as the vector storing the words*/

	int temp=0;
	bool back=0;
	vector<int> random_storage(range);

	for(int q=0;q<range;q++)
	random_storage[q]=0;

	srand(time(0));

	for (int i=0;i<range;i++)
	{
		temp=(rand()%range)+1;
		for(int y=0;y<range;y++)
		{
			if(random_storage[y]==temp)
			{
				back=1;
				break;
			}
			else
			{
				back=0;
			}
		}
		if(back==0)
		{
			random_storage[i]=temp;
		}
		else
			i--;
	}

	if(count>=random_storage.size())
	{
		count=random_storage.size();
	}
	for(unsigned int t=0;t<count;t++)
	{
	//cout<<random_storage[t]<<" ";
	cout<<words[random_storage[t]-1]<<"\n";
	}
	return 0;
}

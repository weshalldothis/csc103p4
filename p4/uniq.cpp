#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
#include <iomanip>
using std::cin;
using std::cout;
using std::getline;
using std::setw;
#include <map>
using std::map;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	//TEST1
/* TODO: write me... */
	string s;
	//int counter=0;
	#if 0
	if(showcount)
	{
		map<string,int> F;
		while(getline(cin, s))
		{
			F[s]++;
		}
	for (map<string,int>::iterator i = F.begin(); i != F.end(); i++)
		cout << (*i).second << ":\t" << (*i).first << "\n";
	}

	if(dupsonly){
		map<string,int> F;
		while(getline(cin, s))
		{
			F[s]++;
		}
	for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
		if((*i).second > 1)
		cout << "\n" << (*i).first << "\n";
	}
	}

	if(uniqonly){
		map<string,int> F;
		while(getline(cin, s))
		{
			F[s]++;
		}
	for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
		if((*i).second == 1)
		cout << "\n" << (*i).first << "\n";
	}
	}
	#endif
	map<string, int> F;
	while(getline(cin,s))
	{
		F[s]++;
	}


	if(!showcount && !dupsonly && !uniqonly)
	{
		for (map<string,int>::iterator i = F.begin(); i != F.end(); i++)
		cout << (*i).first << "\n";
		//print out all lines just once
	}
	if(showcount && dupsonly && !uniqonly)
	{
		for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
			if((*i).second == 1){
				cout << "\n";
			}
			else if((*i).second > 1){
			cout << setw(7) << (*i).second << " " << (*i).first << "\n";
			}
		}
	}

	if(showcount && uniqonly && !dupsonly)
	{
		for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
			if((*i).second > 1){
				cout << "\n";
			}
			else if((*i).second == 1){
			cout << setw(7) << (*i).second << " " << (*i).first << "\n";
			}
		}
	}


	if(showcount && !dupsonly && !uniqonly)
	{
		for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
		cout << setw(7) << (*i).second << " " << (*i).first << "\n";
		//Print count before each line
		}
	}

	if(dupsonly && !uniqonly && !showcount)
	{
		for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
		if((*i).second > 1)
		cout << (*i).first << "\n";
		//Print duplicate lines
	}
}

	if(uniqonly && !showcount && !dupsonly)
	{
		for (map<string,int>::iterator i = F.begin(); i != F.end(); i++){
		if((*i).second == 1)
		cout << (*i).first << "\n";
		//print unique lines
	}
	}

	return 0;
}

